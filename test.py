#! /usr/bin/env python

# pylint: disable=missing-docstring, wildcard-import, unused-wildcard-import

from unittest import main

from tests import *

if __name__ == "__main__":
    main()
