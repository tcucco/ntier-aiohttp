# pylint: disable=missing-docstring
import json

from aiohttp.test_utils import AioHTTPTestCase, unittest_run_loop
from aiohttp.web import Application, middleware
from ntier import (APITransactionBase, PolicyResult, TransactionData,
                   TransactionResult)
from ntier_aiohttp import Deserialize as D
from ntier_aiohttp.connect import execute_transaction
from webdi import ContainerDefinition


class ConnectTest(AioHTTPTestCase):
    async def get_application(self):
        app = Application(middlewares=[container_middleware])
        app["container"] = boot_container()
        app.router.add_get(
            r"/users/{user_id:\d+}",
            transaction_request(
                "get_user_transaction",
                D.map([D.match_info("user_id", D.int)]),
                lambda c: c,
            ),
        )
        app.router.add_get(
            r"/users",
            transaction_request(
                "get_user_list_transaction",
                D.map(
                    [D.query_item("error", D.int)],
                ),
                lambda c: c,
            ),
        )
        app.router.add_put(
            r"/users/{user_id:\d+}",
            transaction_request(
                "update_user_transaction",
                D.map(
                    [
                        D.match_info("user_id", D.int),
                        D.body_prop(
                            "user",
                            D.map({"id": D.int, "name": D.text, "email": D.text}),
                        ),
                    ],
                ),
                lambda c: c,
            ),
        )
        return app

    @unittest_run_loop
    async def test_get_list(self):
        resp = await self.client.request("GET", "/users?page=2&per_page=50")
        self.assertEqual(resp.status, 200)
        body = await resp.json()
        self.assertIn("data", body)
        self.assertEqual(
            body["data"],
            {
                "users": [
                    {"id": 1, "name": "Trey Cucco"},
                ],
            },
        )
        self.assertIn("paging", body)
        self.assertEqual(
            body["paging"],
            {
                "page": 2,
                "per_page": 50,
                "total_records": 51,
                "total_pages": 2,
            },
        )

    @unittest_run_loop
    async def test_get_list_non_int_paging(self):
        resp = await self.client.request("GET", "/users?page=a&per_page=b")
        body = await resp.json()
        self.assertEqual(
            body["paging"],
            {
                "page": 1,
                "per_page": 25,
                "total_records": 51,
                "total_pages": 3,
            },
        )

    @unittest_run_loop
    async def test_get_list_no_page(self):
        resp = await self.client.request("GET", "/users?per_page=25")
        body = await resp.json()
        self.assertEqual(
            body["paging"],
            {
                "page": 1,
                "per_page": 25,
                "total_records": 51,
                "total_pages": 3,
            },
        )

    @unittest_run_loop
    async def test_get_list_no_paging(self):
        resp = await self.client.request("GET", "/users?page=2")
        body = await resp.json()
        self.assertEqual(
            body["paging"],
            {
                "page": 2,
                "per_page": 25,
                "total_records": 51,
                "total_pages": 3,
            },
        )

    @unittest_run_loop
    async def test_get(self):
        resp = await self.client.request("GET", "/users/42")
        self.assertEqual(resp.status, 200)
        body = await resp.json()
        self.assertEqual(
            body["data"],
            {
                "user": {"id": 42, "name": "Trey Cucco"},
            },
        )

    @unittest_run_loop
    async def test_get_list_error(self):
        resp = await self.client.request("GET", "/users?error=1")
        self.assertEqual(resp.status, 403)
        body = await resp.json()
        self.assertIn("errors", body)
        self.assertEqual(
            body["errors"],
            {
                "": [
                    "you do not have permission to view user lists",
                ],
            },
        )

    @unittest_run_loop
    async def test_put(self):
        data = json.dumps(
            {"user": {"name": "Frank Cucco III", "email": "example@example.org"}}
        )
        resp = await self.client.put(
            "/users/13",
            data=data.encode(),
        )
        self.assertEqual(resp.status, 200)
        body = await resp.json()
        self.assertEqual(
            body["data"],
            {
                "user": {
                    "id": 13,
                    "name": "Frank Cucco III",
                    "email": "example@example.org",
                },
            },
        )


def transaction_request(transaction_name, deserializer, serializer):
    async def execute(request):
        return await execute_transaction(
            transaction_name, deserializer, serializer, request["container"], request
        )

    return execute


@middleware
async def container_middleware(request, handler):
    request["container"] = request.app["container"].get_container()
    return await handler(request)


def boot_container():
    container = ContainerDefinition()
    container.add_service("get_user_list_transaction", [], GetUserListTransaction)
    container.add_service("get_user_transaction", [], GetUserTransaction)
    container.add_service("update_user_transaction", [], UpdateUserTransaction)
    return container


class GetUserListTransaction(APITransactionBase):
    async def perform(self, data: TransactionData) -> TransactionResult:
        if data.input.get("error") == 1:
            return TransactionResult.not_authorized(
                PolicyResult.failed("you do not have permission to view user lists"),
            )
        result = TransactionResult.success({"users": [{"id": 1, "name": "Trey Cucco"}]})
        return self.set_output_paging(result, 51)


class GetUserTransaction(APITransactionBase):
    async def perform(self, data: TransactionData) -> TransactionResult:
        user_id = data.input["user_id"]
        return TransactionResult.success(
            {"user": {"id": user_id, "name": "Trey Cucco"}}
        )


class UpdateUserTransaction(APITransactionBase):
    async def perform(self, data: TransactionData) -> TransactionResult:
        user_id = data.input["user_id"]
        user_data = data.input["user"]
        user_data["id"] = user_id
        return TransactionResult.success({"user": user_data})
