# pylint: disable=missing-docstring, no-self-use

from unittest import TestCase

from multidict import MultiDict
from ntier_aiohttp.deserialize import Deserialize as D
from ntier_aiohttp.deserialize import MapDef, RequestData


class DeserializeTest(TestCase):
    def extract(self, data: RequestData, map_def: MapDef):
        data = map_def.extract(data)
        return map_def.map(data)

    def test_query_item(self):
        data = RequestData(MultiDict([("a", "1"), ("b", "2"), ("a", "3")]), {}, {})
        map_def = D.query_item("a", D.int)
        self.assertEqual(self.extract(data, map_def), 1)

    def test_query_list(self):
        data = RequestData(MultiDict([("a", "1"), ("b", "2"), ("a", "3")]), {}, {})
        map_def = D.query_list("a", D.int)
        self.assertEqual(self.extract(data, map_def), [1, 3])

    def test_match_info(self):
        data = RequestData(MultiDict(), {"user_id": "42"}, {})
        map_def = D.match_info("user_id", D.int)
        self.assertEqual(self.extract(data, map_def), 42)

    def test_body(self):
        data = RequestData(MultiDict(), {}, {"id": "1", "name": "Severus Snape"})
        map_def = D.body(D.map({"id": D.int, "name": D.text}), "user")
        self.assertEqual(
            self.extract(data, map_def), {"id": 1, "name": "Severus Snape"}
        )

    def test_body_prop(self):
        data = RequestData(
            MultiDict(), {}, {"user": {"id": "1", "name": "Severus Snape"}}
        )
        map_def = D.body_prop("user", D.map({"id": D.int, "name": D.text}))
        self.assertEqual(
            self.extract(data, map_def), {"id": 1, "name": "Severus Snape"}
        )

    def test_body_path(self):
        data = RequestData(
            MultiDict(), {}, {"user": {"id": "1", "name": "Severus Snape"}}
        )
        map_def = D.body_path(["user", "id"], D.int, "user_id")
        self.assertEqual(self.extract(data, map_def), 1)

    def test_body_path_fails(self):
        data = RequestData(
            MultiDict(), {}, {"user": {"id": "1", "name": "Severus Snape"}}
        )
        map_def = D.body_path(
            ["user", "email"], D.text, "user_email", "someone@example.com"
        )
        self.assertEqual(self.extract(data, map_def), "someone@example.com")
